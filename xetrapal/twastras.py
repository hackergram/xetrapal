#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 00:25:58 2018

@author: ananda
"""

import twython
from . import astra
import colored
import json
import os
from datetime import datetime
import tweepy
from . import karma


class XpalTwitterStreamer(twython.TwythonStreamer):
    def __init__(self, ofile, tracklength=None, logger=astra.baselogger, *args, **kwargs):
        super(XpalTwitterStreamer, self).__init__(*args, **kwargs)
        self.ofile = ofile
        self.buffer = []
        self.logger = logger
        self.tracklength = tracklength

    def flush_buffer(self):
        ofilejson = []
        if os.path.exists(self.ofile):
            with open(self.ofile, "r") as f:
                ofilejson = json.loads(f.read())
        ofilejson += self.buffer
        with open(self.ofile, "w") as f:
            f.write(json.dumps(ofilejson))
        self.buffer = []
        if self.tracklength is not None:
            if len(ofilejson) > self.tracklength:
                self.disconnect()

    def on_success(self, data):
        # self.logger.info(data)
        self.buffer.append(data)
        if len(self.buffer) > 10:
            self.flush_buffer()

    def on_error(self, status_code, data):
        print(status_code)

        # Want to stop trying to get data because of the error?
        # Uncomment the next line!
        # self.disconnect()


class XpalTwitterSheeter(XpalTwitterStreamer):
    def __init__(self, sheetname, logger, *args, **kwargs):
        super(XpalTwitterStreamer, self).__init__(*args, **kwargs)
        # self.


def tw_get_twython_streamer(config=None, tracklength=None, path=None, ofilename=None, logger=astra.baselogger, **kwargs):
    twconfig = karma.load_config(config.get("Twitter", "authfile"))
    if ofilename is None:
        ts = datetime.now()
        ofilename = "TwythonStreamer-" + \
            ts.strftime("%Y%b%d-%H%M%S" + ".json")
    if path is None:
        path = "/tmp"
    ofilename = os.path.join(path, ofilename)
    logger.info("Trying to get a twython streamer to work with twitter streams to log at {}".format(ofilename))
    app_key = twconfig.get("Twitter", 'app_key')
    app_secret = twconfig.get("Twitter", 'app_secret')
    oauth_token = twconfig.get("Twitter", 'oauth_token')
    oauth_token_secret = twconfig.get("Twitter", 'oauth_token_secret')
    try:
        t = XpalTwitterStreamer(
            ofilename, tracklength, logger, app_key, app_secret, oauth_token, oauth_token_secret)
        logger.info("Streamer logging at "
                    + colored.stylize(t.ofile, colored.fg("yellow")))
        return t
    except Exception as e:
        logger.error("Could not get twython streamer because %s" % repr(e))
        return None

# Get a Twython to work with twitter


def tw_get_twython(config=None, logger=astra.baselogger, **kwargs):
    twconfig = karma.load_config(config.get("Twitter", "authfile"))
    logger.info("Trying to get a twython to work with twitter")
    app_key = twconfig.get("Twitter", 'app_key')
    app_secret = twconfig.get("Twitter", 'app_secret')
    oauth_token = twconfig.get("Twitter", 'oauth_token')
    oauth_token_secret = twconfig.get("Twitter", 'oauth_token_secret')
    try:
        t = twython.Twython(app_key, app_secret, oauth_token, oauth_token_secret)
        return t
    except Exception as e:
        logger.error("Could not get twython config because %s" % repr(e))
        return None


def tw_get_tweepy(config=None, logger=astra.baselogger, **kwargs):
    twconfig = karma.load_config(config.get("Twitter", "authfile"))
    logger.info("Trying to get a tweepy to work with twitter")
    app_key = twconfig.get("Twitter", 'app_key')
    app_secret = twconfig.get("Twitter", 'app_secret')
    oauth_token = twconfig.get("Twitter", 'oauth_token')
    oauth_token_secret = twconfig.get("Twitter", 'oauth_token_secret')
    auth = tweepy.OAuthHandler(app_key, app_secret)
    auth.set_access_token(oauth_token, oauth_token_secret)
    try:
        tweep = tweepy.API(auth)
        return tweep
    except Exception as e:
        logger.error("Could not get a tweepy because %s" % repr(e))
