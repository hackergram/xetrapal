# coding: utf-8
import time
import datetime
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from tqdm import tqdm
'''
यहां हम फेसबुक सम्बन्धी अस्त्रों का उल्लेख करेंगे
'''
# from .astra import *
from . import astra
from . import karma
from . import fbsmriti

# Fire and Forget Astras, to be run with {'msg':'run','func':function_object,'args':(),'kwargs':{}}

FBCLASSMAP = {
    "post-content": "//div[contains(@class,'userContentWrapper')]",
    "profile-link": "//a[@class='profileLink']",
    "post-link": "//a[@class='_5pcq']",
    "status-update": "//div[@data-testid='status-attachment-mentions-input']",
    "status-post-button": "//button[@data-testid='react-composer-post-button']",
    "status-close": "//div[@data-testid='react-composer-close-button']"
    }


def fb_get_element(element=None, multi=False, fbbrowser=None, logger=astra.baselogger, **kwargs):
    '''
    Gets a specified element from a facebook web signed in browser
    Arguments:
        element - the name of the element to be looked up in WAWCLASSMAP
        multi - specifies whether to get multiple elements corresponding to the xpath specified (True if multiple)
        fbbrowser - handle to a facebook signed in browser session, use fb_test_login to test if logged in
        logger - which logger to write output to
    Returns:
        A selenium web element if successful, None if fails
    '''
    try:
        if multi is False:
            return fbbrowser.find_element_by_xpath(FBCLASSMAP[element])
        else:
            return fbbrowser.find_elements_by_xpath(FBCLASSMAP[element])
    except Exception as e:
        logger.error("{} {}".format(type(e), str(e)))
        return None


def fb_click_element(element=None, fbbrowser=None, logger=astra.baselogger, **kwargs):
    '''
    Clicks a specified element from a facebook signed in browser
    Arguments:
        element - the name of the element to be looked up in FBCLASSMAP
        fbbrowser - handle to a facebook signed in browser session, use fb_test_login to test if logged in
        logger - which logger to write output to
    Returns:
        True if successful, False if fails
    '''

    try:
        elem = fb_get_element(element=element, fbbrowser=fbbrowser, logger=logger, **kwargs)
        if elem is not None:
            try:
                fbbrowser.execute_script("arguments[0].scrollIntoView(true)", elem)
            except Exception as e:
                logger.error("Could not scroll element into view")
            elem.click()
            logger.info("Clicked element {}".format(element))
            return True
        else:
            logger.error("Could not find element {}".format(element))
            return False
    except Exception as e:
        logger.error("{} {}  in trying to click the element {}".format(type(e), str(e), element))
        return False


def fb_send_keys_to_element(element=None, keys=None, clearfirst=False, fbbrowser=None, logger=astra.baselogger, **kwargs):
    '''
    Sends specified keys to a specified element from a whatsapp web signed in browser
    Arguments:
        element - the name of the element to be looked up in WAWCLASSMAP
        keys - keys to send, can be a string or a valid selenium.webdriver.common.keys Keys member
        fbbrowser - handle to a whatsapp signed in browser session, use fb_test_login to test if logged in
        logger - which logger to write output to
    Returns:
        True if successful, False if fails
    '''
    try:
        elem = fb_get_element(element=element, fbbrowser=fbbrowser, logger=logger, **kwargs)
        if elem is not None:
            if clearfirst:
                elem.clear()
            elem.send_keys(keys)
            logger.info("Sent keys '{}' to element {}".format(keys, element))
            return True
        else:
            logger.error("Could not locate element {}".format(element))
            return False
    except Exception as e:
        logger.error("{} {} sending keys {} to element {}".format(type(e), str(e), keys, element))
        return False


def fb_login(fbbrowser=None, config=None, fbusr=None, fbpwd=None, logger=astra.baselogger, **kwargs):
    if fbusr is None or fbpwd is None:
        fbusr = config.get("Facebook", "fbusername")
        fbpwd = config.get("Facebook", "fbpassword")
    # or you can use Chrome(executable_path="/usr/bin/chromedriver")
    logger.info("Trying to log into FB in browser...")
    try:
        fbbrowser.get("http://www.facebook.com")
        assert "Facebook" in fbbrowser.title
        elem = fbbrowser.find_element_by_id("email")
        elem.send_keys(fbusr)
        elem = fbbrowser.find_element_by_id("pass")
        elem.send_keys(fbpwd)
        elem.send_keys(Keys.RETURN)
        time.sleep(10)
        fbbrowser.get("http://facebook.com/profile.php")
        time.sleep(10)
        logger.info("Successfully logged into FB")
    except Exception as exception:
        logger.error("Could not log into FB.." + repr(exception))


def fb_search(searchstring, fbbrowser=None, logger=astra.baselogger, **kwargs):
    logger.info("Searching FB for " + searchstring)
    searchbar = fbbrowser.find_element_by_name("q")
    searchbar.clear()
    searchbar.send_keys(searchstring)
    searchbar.send_keys(Keys.ENTER)
    time.sleep(10)


def fb_add_get_post_smriti(postdict, logger=astra.baselogger, *args, **kwargs):
    if "url" in postdict.keys():
        fbpost = fbsmriti.FacebookPost.objects.filter(url=postdict['url'])
    else:
        fbpost = fbsmriti.FacebookPost.objects.filter(**postdict)
    if len(fbpost):
        return fbpost
    else:
        try:
            fbpost = fbsmriti.FacebookPost(**postdict)
            fbpost.save()
            fbpost.reload()
        except Exception as e:
            logger("Error {} {} adding post {}".format(type(e), str(e), postdict))
            return "Error {} {} adding post {}".format(type(e), str(e), postdict)
    return [fbpost]


def fb_add_get_profile_smriti(profiledict, logger=astra.baselogger, *args, **kwargs):
    if "url" in profiledict.keys():
        fbprofile = fbsmriti.FacebookProfile.objects.filter(url=profiledict['url'])
    else:
        fbprofile = fbsmriti.FacebookProfile.objects.filter(**profiledict)
    if len(fbprofile):
        fbprofile = fbprofile[0]
    else:
        try:
            fbprofile = fbsmriti.FacebookProfile(**profiledict)
            fbprofile.save()
            fbprofile.reload()
        except Exception as e:
            logger("Error {} {} adding post {}".format(type(e), str(e), profiledict))
            return "Error {} {} adding post {}".format(type(e), str(e), profiledict)
    return [fbprofile]


def fb_get_posts_from_timeline(url="https://facebook.com", fbbrowser=None, count=10, logger=astra.baselogger, **kwargs):
    fbbrowser.get(url)
    karma.wait()
    postlinks = []

    while len(postlinks) < count:
        posts = []
        postcontents = []
        posts = posts + \
            fbbrowser.find_elements_by_class_name("userContentWrapper")
        for post in posts:
            postcontents.append(BeautifulSoup(post.get_property("innerHTML")))
        for postcontent in postcontents:
            try:
                pc = list(postcontent.find_all("a", {"class": "_5pcq"}))
                if len(pc) > 0:
                    pc = pc[0]
                else:
                    continue
                url = "https://facebook.com" + pc.get("href")
                logger.info("Logging post at {}".format(
                    pc.find("abbr").get("title")))
                postlink = {}
                postlink['text'] = postcontent.text
                logger.info(postcontent)
                postlink['url'] = url
                postlink['created_timestamp'] = datetime.datetime.strptime(
                    pc.find("abbr").get("title"), "%m/%d/%y, %I:%M %p")
                if url not in [pl['url'] for pl in postlinks]:
                    postlinks.append(postlink)
            except Exception as e:
                logger.error("Could not parse post {} {}".format(type(e), str(e)))
        logger.info("Got %s posts " % (len(postlinks)))
        # postlinks = list(set(postlinks))
        if "https://facebook.com#" in postlinks:
            postlinks.pop(postlinks.index("https://facebook.com#"))
        karma.scroll_page(fbbrowser)
        karma.wait()
    return postlinks[:count]


def fb_get_els_from_timeline(url="https://facebook.com", element="post-content",fbbrowser=None, count=10, logger=astra.baselogger, **kwargs):
    fbbrowser.get(url)
    karma.wait()
    postlinks = []
    while len(postlinks) < count:
        postlinks = postlinks + fb_get_element(element, multi=True, fbbrowser=fbbrowser)
        logger.info("Got %s posts " % (len(postlinks)))
        postlinks = list(set(postlinks))
        karma.scroll_page(fbbrowser)
        karma.wait()
    return postlinks[:count]


def fb_get_profile_data(url, fbbrowser=None, logger=astra.baselogger, **kwargs):
    profiledata = {}
    profilepic = {}
    fbbrowser.get(url)
    karma.wait()
    profile = fbbrowser.current_url

    if profile == "http://www.facebook.com/profile.php":
        plink = fbbrowser.find_element_by_xpath("//a[@title='Profile']")
        profile = plink.get_attribute("href")

    profiledata['url'] = profile
    '''
    if "?" in profiledata['url']:
        baselink = profiledata['url'].split("?")[0]
        profiledata['url'] = baselink
    '''
    # plink=fbbrowser.find_element_by_xpath("//a[@title='Profile']")
    try:
        profiledata['fbdisplayname'] = fb_get_cur_page_displayname(fbbrowser)
    except Exception as e:
        logger.error("Error {} {} trying to get display name".format(type(e), str(e)))
    try:
        profilepicthumb = fbbrowser.find_element_by_class_name("profilePicThumb")
        profilepic['url'] = profilepicthumb.get_attribute("href")
        '''
        if "?" in profilepic['url']:
            baselink = profilepic['url'].split("?")[0]
            profilepic['url'] = baselink
        '''
        time.sleep(10)
    except Exception as e:
        logger.error("Error {} {} trying to click profile pic thumbnail".format(type(e), str(e)))
    '''
    try:
        profilepic['alttext'] = fbbrowser.find_element_by_class_name(
            "spotlight").get_property("alt")
        profilepic['src'] = fbbrowser.find_element_by_class_name(
            "spotlight").get_property("src")
        profilepic['profileguard'] = False
    except Exception as e:
        logger.error("{} {}".format(type(e), str(e)))
        profilepic['alttext'] = fbbrowser.find_element_by_class_name(
            "profilePicThumb").get_property("alt")
        profilepic['src'] = fbbrowser.find_element_by_class_name(
            "profilePic").get_property("src")
        profilepic['profileguard'] = True
    try:
        localfile = karma.download_file(
            profilepic['src'], prefix=profiledata['fbdisplayname'].replace(" ", ""), suffix=".jpg")
        profilepic['localfile'] = localfile

    except Exception as e:
        logger.error("Failed to download {} {}".format(type(e), str(e)))
    '''
    try:
        profiledata['tabdata'] = fb_get_profile_tab_data(profileurl=url, fbbrowser=fbbrowser)
        profiledata['friendcount'] = profiledata['tabdata']['friends']['count']
        profiledata['profilepic'] = profilepic
    except Exception as e:
        logger.error("Could not get profile meta fully")
    return profiledata


def fb_get_profile_tab_data(profileurl, fbbrowser=None, **kwargs):
    tabdata = {"friends": {}, "photos": {}, "about": {}}
    fbbrowser.get(profileurl)
    time.sleep(5)
    phototab = fbbrowser.find_element_by_xpath(
        "//a[@data-tab-key='photos']").get_property("href")
    friendtab = fbbrowser.find_element_by_xpath(
        "//a[@data-tab-key='friends']").get_property("href")
    abouttab = fbbrowser.find_element_by_xpath(
        "//a[@data-tab-key='about']").get_property("href")
    tabdata['friends']['url'] = friendtab
    if fbbrowser.find_element_by_xpath("//a[@data-tab-key='friends']").get_property("text").replace("Friends", "") != "" and "Mutual" not in fbbrowser.find_element_by_xpath("//a[@data-tab-key='friends']").get_property("text"):
        tabdata['friends']['count'] = int(fbbrowser.find_element_by_xpath(
            "//a[@data-tab-key='friends']").get_property("text").replace("Friends", ""))
    else:
        tabdata['friends']['count'] = -1
    tabdata['photos']['url'] = phototab
    tabdata['about']['url'] = abouttab
    return tabdata


def fb_get_cur_page_displayname(fbbrowser=None, **kwargs):
    displayname = fbbrowser.find_element_by_id(
        "fb-timeline-cover-name").find_element_by_tag_name("a").text
    return displayname


def fb_like_page_toggle(pageurl, fbbrowser=None, **kwargs):
    fbbrowser.get(pageurl)
    likebutton = fbbrowser.find_element_by_xpath(
        "//button[@data-testid='page_profile_like_button_test_id']")
    likebutton.click()


def fb_post(post, fbbrowser=None, **kwargs):
    fbbrowser.get("https://facebook.com/profile")
    karma.wait(waittime="short")
    fbbrowser.find_element_by_xpath(
        "//div[@data-testid='status-attachment-mentions-input']").click()
    karma.wait()
    fbbrowser.find_element_by_xpath(
        "//div[@data-testid='status-attachment-mentions-input']").send_keys(post['text'])
    karma.wait()
    fbbrowser.find_element_by_xpath(
        "//button[@data-testid='react-composer-post-button']").click()
    karma.wait()
    return "Message posted successfully"

def fb_get_reacters(url,count=5, fbbrowser=None, **kwargs):
    fbbrowser.get(url)
    reactlist=fbbrowser.find_elements_by_xpath("//a[@data-testid='UFI2ReactionsCount/root']")
    friendlist=[]
    for i in range(count):
        for reactions in reactlist:
            try:
                try:
                    fbbrowser.find_element_by_link_text("Close").click()
                except Exception as e:
                    print(e)
                karma.wait()
                reactions.click()
                karma.wait(waittime="short")
                flist=fbbrowser.find_element_by_xpath("//ul[contains(@id,'reaction_profile_browser')]").find_elements_by_tag_name("li")
                for f in flist:
                    num_mutual=0
                    uid=""
                    messageurl=""
                    for a in f.find_elements_by_tag_name("a"):
                        if a.text != "" and a.text != "Message":
                            print(a.text)
                            if "mutual" not in a.text:
                                name=a.text
                                profileurl=a.get_attribute("href").split("?fref")[0]
                            if "mutual" in a.text:
                                num_mutual=int(a.text.split(" ")[0])
                                uid=a.get_attribute("href").split("?uid=")[1]
                        if a.text=="Message":
                                messageurl=a.get_attribute("href")
                    print(name,profileurl,num_mutual,messageurl)
                    if profileurl not in list(set([react['profileurl'] for react in friendlist])):
                        dictionary={"name":name,"profileurl":profileurl,"num_mutual":num_mutual,"messageurl":messageurl,"uid":uid}
                        friendlist.append(dictionary)
            except Exception as e:
                print(e)
            try:
                fbbrowser.find_element_by_link_text("Close").click()
            except Exception as e:
                print(e)
        reactlistold=reactlist
        karma.scroll_page(browser=fbbrowser)
        karma.scroll_page(browser=fbbrowser)
        reactlist=fbbrowser.find_elements_by_xpath("//a[@data-testid='UFI2ReactionsCount/root']")
        for item in reactlistold:
            if item in reactlist:
                reactlist.remove(item)
    return friendlist

def fb_get_open_friendlist(url, fbbrowser=None, **kwargs):
    fbbrowser.get(url)
    karma.wait()
    fbbrowser.find_element_by_xpath("//a[@data-tab-key='friends']").click()
    karma.scroll_to_bottom(fbbrowser)
    soup=BeautifulSoup(fbbrowser.find_element_by_id("pagelet_timeline_medley_friends").get_attribute("innerHTML"))
    listoflists=soup.find_all("ul")
    friendlist=[]
    for l in tqdm(listoflists):
        for n in l.find_all("li"):
            furl=n.find("a").get("href").split("?fref")[0]
            furl=furl.split("&fref")[0]
            text="\n".join([t.text for t in n.find_all("a")]).replace("Friends","").replace("Friend","")
            if "/pages/" not in furl:
                dictionary={"url":furl,"name":text}
                tqdm.write(str(dictionary))
                if furl not in [a['url'] for a in friendlist]:
                    friendlist.append(dictionary)
    return friendlist
