import requests
from . import astra


def send_samvad(samvadurl=None, message=None, logger=astra.baselogger, *args, **kwargs):
    try:
        logger.info("Sending samvad {}".format(message))
        r = requests.post(url=samvadurl, json=message)
        return r.json()
    except Exception as e:
        return "{} {}".format(type(e), str(e))


def get_aadesh(samvadurl=None, message=None, *args, **kwargs):
    try:
        r = requests.post(url=samvadurl, data=message)
        return r
    except Exception as e:
        return "{} {}".format(type(e), str(e))
