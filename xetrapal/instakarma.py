from . import astra
from . import karma
from . import aadhaar
# from bs4 import BeautifulSoup
import os
import datetime
from copy import deepcopy
from . import wasmriti
import pandas
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import random
INSTACLASSMAP={

}

def insta_login(instabrowser=None, config=None, logger=astra.baselogger, **kwargs):
    fbusr = config.get("Facebook", "fbusername")
    fbpwd = config.get("Facebook", "fbpassword")
    # or you can use Chrome(executable_path="/usr/bin/chromeinstabrowser")
    logger.info("Trying to log into instagram in browser...")
    try:
        instabrowser.get("http://www.instagram.com")
        time.sleep(10)
        login_button=instabrowser.find_elements_by_class_name("Igw0E")
        login_button[0].click()
        assert "Facebook" in instabrowser.title
        elem = instabrowser.find_element_by_name("email")
        elem.send_keys(fbusr)
        elem = instabrowser.find_element_by_name("pass")
        elem.send_keys(fbpwd)
        elem.send_keys(Keys.RETURN)
        instabrowser.send_keys(Keys.CONTROL,Keys.SHIFT,"m")
        time.sleep(10)
        instabrowser.get("http://instagram.com")
        time.sleep(10)
        logger.info("Successfully logged into Instagram")
    except Exception as exception:
        logger.error("Could not log into Instagram" + repr(exception))


def insta_like_all(instabrowser=None,config=None,logger=astra.baselogger,**kwargs):
    try:


        SCROLL_PAUSE_TIME = 10

        base_height=0
        last_height =800
        instabrowser.execute_script("window.scrollTo(0,500);")
        post1=instabrowser.find_element_by_tag_name("article")
        like_button1=post1.find_element_by_class_name("fr66n")
        post1.click()
        time.sleep(5)
        like_button1.click()
        time.sleep(5)
        logger.info("Likeing first photo")
        instabrowser.execute_script("window.scrollTo("+str(base_height)+","+str(last_height)+");")
        while True:

            post=instabrowser.find_element_by_tag_name("article")
            if post1!=post:
                like_button=post.find_element_by_class_name("fr66n")
                post.click()
                time.sleep(5)
                like_button.click()
                time.sleep(5)
                logger.info("likeing more photos")
                instabrowser.execute_script("window.scrollTo("+str(base_height)+","+str(last_height)+");")

            # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)

                # Calculate new scroll height and compare with last scroll height
                base_height+=1000
                last_height+=1000
            else:

                instabrowser.execute_script("window.scrollTo("+str(base_height)+","+str(last_height)+");")
                logger.info("scrolling down")
                base_height+=1000
                last_height+=1000

    except Exception as exception:
        logger.error("Not able to like because" + repr(exception))

def insta_post_random_comments(text_file=None,instabrowser=None,config=None,logger=astra.baselogger,**kwargs):

    try:
        comments_file=open(text_file,"r")
        comment_list=comments_file.readlines()
        SCROLL_PAUSE_TIME = 10

        base_height=0
        last_height =800
        instabrowser.execute_script("window.scrollTo(0,700);")
        post1=instabrowser.find_element_by_tag_name("article")
        btn1=post1.find_element_by_xpath("//button[text()='Post']")
        comment1= post1.find_element_by_xpath("//textarea[@placeholder='Add a comment…']")
        post1.click()
        time.sleep(5)
        comment1.click()
        comment1.send_keys(comment_list[random.choice(range(len(comment_list)))])
        time.sleep(5)
        logger.info("Commenting on first photo")
        btn1.click()
        instabrowser.execute_script("window.scrollTo("+str(base_height)+","+str(last_height)+");")
        while True:

            post=instabrowser.find_element_by_tag_name("article")
            if post1!=post:
                btn=post.find_element_by_xpath("//button[text()='Post']")
                comment= post.find_element_by_xpath("//textarea[@placeholder='Add a comment…']")
                post.click()
                time.sleep(5)
                a=ActionChains(instabrowser)
                a.key_down(Keys.DOWN).perform()
                time.sleep(5)
                comment.click()
                comment.send_keys(comment_list[random.choice(range(len(comment_list)))])
                time.sleep(5)
                btn.click()
                time.sleep(5)
                logger.info("Commenting on more photos")
                instabrowser.execute_script("window.scrollTo("+str(base_height)+","+str(last_height)+");")

                # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)

            # Calculate new scroll height and compare with last scroll height
                base_height+=1000
                last_height+=1000
            else:

                instabrowser.execute_script("window.scrollTo("+str(base_height)+","+str(last_height)+");")
                logger.info("scrolling down")
                base_height+=1000
                last_height+=1000

    except Exception as exception:
        logger.error("Not able to like because" + repr(exception))


def insta_get_follwers_list(instabrowser=None,config=None,logger=astra.baselogger,**kwargs):
    instabrowser.find_element_by_xpath("//span[@aria-label='Profile']").click()
    time.sleep(10)
    instabrowser.find_element_by_xpath("//a[@href='/karanjoshi1993/followers/']").click()
    time.sleep(5)
    insta_id=instabrowser.find_elements_by_class_name("FPmhX")
    id_list=[id.text for id in insta_id]
    print(id_list)
    insta_name=instabrowser.find_elements_by_class_name("wFPL8")
    name_list=[name.text for name in insta_name]
    print(name_list)
    return dict(zip(name_list,id_list))

def insta_get_follwing_list(instabrowser=None,config=None,logger=astra.baselogger,**kwargs):
    instabrowser.find_element_by_xpath("//span[@aria-label='Profile']").click()
    time.sleep(10)
    instabrowser.find_element_by_xpath("//a[@href='/karanjoshi1993/following/']").click()
    time.sleep(10)
    insta_id=instabrowser.find_elements_by_class_name("FPmhX")
    id_list=[id.text for id in insta_id]
    insta_name=instabrowser.find_elements_by_class_name("wFPL8")
    name_list=[name.text for name in insta_name]
    return dict(zip(name_list,id_list))

def insta_get_follwing_list(text, name, instabrowser=None,config=None,logger=astra.baselogger,**kwargs):
    instabrowser.find_element_by_xpath("//span[@aria-label='Direct']").click()
    try:
        instabrowser.find_element_by_xpath("//div[text()='"+id+"']").click()
        time.sleep(5)
        instabrowser.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        message_box=instabrowser.find_element_by_tag_name("textarea")
        message_box.click()
        message_box.send_keys(text)
        instabrowser.find_element_by_xpath("//button[text()='Send']")
    except Exception as exception:
        logger.error("Not able to send message because" + repr(exception))
