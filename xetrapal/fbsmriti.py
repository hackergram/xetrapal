#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on 2019-01-09

@author: arjunvenkatraman
"""

from mongoengine import fields, DynamicDocument
# from . import aadhaar
from . import smriti
# from samvad import utils
# import json
# import bson


class FacebookProfile(smriti.SmritiBase, DynamicDocument):
    url = fields.StringField(unique=True, required=False, sparse=True)
    sandesh_type = fields.ListField(default=["facebook_post", "facebook_comment"])


class FacebookTimeline(smriti.SmritiBase, DynamicDocument):
    url = fields.StringField(unique=True, required=False, sparse=True)
    sandesh_type = fields.ListField(default=["facebook_post"])


class FacebookPost(smriti.SmritiBase, DynamicDocument):
    text_lines = fields.ListField()
    observed_in = fields.StringField()
    sent_by = fields.StringField()
    url = fields.StringField(unique=True, required=False, sparse=True)
    sandesh_type = fields.ListField(default=["facebook_comment"])
    sender_type = fields.StringField(default="facebook_profile")


class FacebookComment(smriti.SmritiBase, DynamicDocument):
    text_lines = fields.ListField()
    observed_on = fields.StringField()
    sent_by = fields.StringField()
    sender_type = fields.StringField(default="facebook_profile")
