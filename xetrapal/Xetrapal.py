# coding: utf-8
'''
            __________
        < Xetrapal (क्षेत्रपाल) >
            ----------
                \   ^__^
                 \  (oo)\_______
                    (__)\       )\/\
                        ||----w |
                        ||     ||

        हिन्दी में सोशियल मीडिया का अध्ययन
        यह एक इनिट फ़ाइल है, जो कि पाइथन के हर पैकेज में होनी अनिवार्य है।

'''


from . import astra
from . import karma
from . import jeeva
from . import aadhaar

# from . import fbkarmas
# from . import twkarmas
# from . import wakarmas
# from . import gdkarmas
import colored
# from . import twastras
# from . import gdastras
# from . import smsastras
# from . import aadesh
from inspect import getmembers, isfunction
import json
# import gdkarmas
from . import pykkakarta
# import os
# import mojomailastras
# from . import telegramastras
# import telegramkarmas
# import thespiankarta


class Xetrapal(jeeva.Jeeva):
    def __init__(self, *args, **kwargs):
        super(Xetrapal, self).__init__(*args, **kwargs)
        self.vaahans = {}
        self.astras = {}
        self.karmas = []
        # self.save_profile()
        self.dhaarana(astra)
        self.dhaarana(karma)
        self.update_astras()
        self.update_vaahans()

    def help(self, *args, **kwargs):
        if len(args) == 0:
            print(colored.stylize(aadhaar.bannertext, colored.fg("violet")))
            print(colored.stylize(aadhaar.helptext, colored.fg("red")))
            print("This instance details:\n {}".format(colored.stylize(repr(self.smriti), colored.fg("yellow"))))
            print("Available functions: ")
            print("\n".join(["{}\n{}".format(colored.stylize(x, colored.fg("red")), colored.stylize(self.__dict__[x].__doc__, colored.fg("blue"))) for x in self.karmas]))

    def update_astras(self):
        self.logger.info("Trying to update astras")
        # astras = {}
        if self.astras == {}:
            self.logger.warning("I dont seem to have any astras")
        '''
        else:
            for astraname in self.astras.keys():
                astras[astraname] = str(type(self.astras[astraname]))
        '''
        # self.set_property("astras", astras)
        self.session.astras = self.astras
        for ast in self.session.astras.keys():
            self.session.astras[ast] = str(self.astras[ast])
        self.session.save()
        self.session.reload()

    def update_vaahans(self):
        self.logger.info("Trying to update vaahans")
        vaahans = {}
        if self.vaahans == {}:
            self.logger.warning("I dont seem to have any vaahans")
        else:
            for vaahanname in self.vaahans.keys():
                vaahans[vaahanname] = str(type(self.vaahans[vaahanname]))

        # self.set_property("vaahans", vaahans)
        self.session.vaahans = self.vaahans
        self.session.save()
        self.session.reload()
        # self.save_profile()

    def add_vaahan(self, vaahan):
        self.vaahans[vaahan.name] = vaahan
        self.update_vaahans()

    def release_vaahan(self, vaahanname):
        self.logger.info("Releasing vaahan " + colored.stylize(vaahanname, colored.fg("violet")))
        vaahan = self.vaahans.pop(vaahanname)
        self.update_vaahans()
        return vaahan

    def add_astra(self, astraname, astracall=None):
        if hasattr(self, astracall):
            newastra = getattr(self, astracall)()
        self.astras[astraname] = newastra
        self.update_astras()
        return newastra

    def drop_astra(self, astraname):
        self.logger.info("Dropping astra " + colored.stylize(astraname, colored.fg("violet")))
        droppedastra = self.astras.pop(astraname)
        self.update_astras()
        return droppedastra
    '''
    def get_aadesh(self, message):
        if isinstance(message, (aadesh.Aadesh)):
            self.logger.info("Received Aadesh " + str(message))
            if message.msg == "run":
                self.logger.info("Trying to run " + str(message.func))
                # message['kwargs']['logger']=self.jeeva.logger
                try:
                    message.kwargs['logger'] = self.logger
                    message.func(*message.args, **message.kwargs)
                except Exception as e:
                    self.jeeva.logger.error(repr(e))
            if message.msg == "get":
                self.logger.info(
                    "Trying to get a return value from " + str(message.func))
                try:
                    # message.kwargs['logger'] = self.logger
                    returnval = message.func(*message.args, **message.kwargs)
                    self.logger.info("Got value {}".format(returnval))
                    return returnval
                except Exception as e:
                    self.logger.error(repr(e))
                    return repr(e)
    '''

    def start_pykka_karta(self):
        kartaref = pykkakarta.Karta.start(jeeva=self)
        self.kartarefs.append(kartaref)
        return kartaref

    def run_aadesh(self, aadesh):
        try:
                k = self.start_pykka_karta()
                aadesh['func'] = getattr(self, aadesh['func'])
                if aadesh['msg'] == "run":
                    k.tell(aadesh)
                    op = "Sent Command"
                else:
                    op = k.ask(aadesh)
                # method = getattr(apixpal, resp[0]['command'])
                # apixpal.logger.info("Running command {}".format(str(method)))
        except Exception as e:
                op = "{} {}".format(str(e), type(e))
        return(op)

    def load_karma(self, func):
        def kriya(*args, **kwargs):
            args = list(args)
            kwargs['logger'] = self.logger
            kwargs['path'] = self.sessiondownloadpath
            if "config" not in kwargs.keys() or kwargs['config'] is None:
                if self.configfile is not None:
                    kwargs['config'] = karma.load_config_json(configfile=self.configfile, logger=self.logger)
            for key in self.astras.keys():
                if key not in kwargs.keys():
                    kwargs[key] = self.astras[key]
                else:
                    kwargs[key] = self.astras[kwargs[key]]
            # print(args, kwargs)
            self.logger.info("Performing karma {}".format(func.__name__))
            try:
                return func(*args, **kwargs)
            except Exception as e:
                self.logger.error("{} {} in function {}".format(type(e), str(e), func))

        kriya.__doc__ = func.__doc__
        return kriya

    def dhaarana(self, module):
        self.logger.info("Trying to load module {}".format(module.__name__))
        try:
            functions_list = [o for o in getmembers(module) if isfunction(o[1])]
            for func in functions_list:
                self.__dict__[func[0]] = self.load_karma(func[1])
            self.karmas += [f[0] for f in functions_list]
            self.karmas = list(set(self.karmas))
            self.karmas.sort()
            self.session.karmas_loaded += self.karmas
            self.session.karmas_loaded = list(set(self.session.karmas_loaded))
            self.session.save()
            self.session.reload()
            self.smriti.reload()
            self.logger.info("I now have functions {}".format(self.karmas))
        except Exception as e:
            self.logger.error("Error loading module {}, {} {}".format(module.__name__, type(e), str(e)))
