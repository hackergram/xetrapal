#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on 2019-01-09
@author: arjunvenkatraman
"""

from mongoengine import fields, DynamicDocument
# rom . import aadhaar
from . import smriti
# from samvad import utils
# import json
# import bson


class TwitterProfile(smriti.SmritiBase, DynamicDocument):
    username = fields.StringField(unique=True, required=False, sparse=True)


class TweetCollection(smriti.SmritiBase, DynamicDocument):
    text_lines = fields.ListField()


class Tweet(smriti.SmritiBase, DynamicDocument):
    text_lines = fields.ListField()
    observed_in = fields.StringField()
    sent_by = fields.StringField()
