#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on 2019-01-09
@author: arjunvenkatraman
"""

from mongoengine import fields, DynamicDocument
# rom . import aadhaar
from . import smriti
# from samvad import utils
# import json
# import bson


class CubeDef(smriti.SmritiBase, DynamicDocument):
    variables = fields.ListField()
