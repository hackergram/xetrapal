#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on 2019-01-09

@author: arjunvenkatraman
"""

from mongoengine import fields, DynamicDocument
# rom . import aadhaar
from . import smriti
# from samvad import utils
# import json
# import bson


class WhatsappProfile(smriti.SmritiBase, DynamicDocument):
    mobile_num = fields.StringField(unique=True, required=False, sparse=True)
    sandesh_type = fields.ListField(default=["whatsapp_message"])


class WhatsappConversation(smriti.SmritiBase, DynamicDocument):
    display_name = fields.StringField(unique=True, required=False, sparse=True)
    display_lines = fields.ListField()
    sandesh_type = fields.ListField(default=["whatsapp_message"])


class WhatsappMessage(smriti.SmritiBase, DynamicDocument):
    text_lines = fields.ListField()
    observed_in = fields.StringField()
    sent_by = fields.StringField()
    sender_type = fields.StringField(default="whatsapp_profile")
