"""
Created on Sat Sep 22 20:54:42 2018

@author: arjun
"""
from flask import Flask, jsonify, request
from flask_cors import CORS
# from flask_mongoengine import MongoEngine
import json
# import datetime
# import datetime
from flask_restful import reqparse, Api, Resource
# from flask_mongoengine import MongoEngine
import copy
import sys
# import urllib
from . import karma, smriti, Xetrapal, wakarmas, fbkarmas, twkarmas, samvadkarmas

# wasmriti, fbsmriti, aadhaar
# from samvad import xpal
# import mongoengine
# mongoengine.disconnect()

app = Flask(__name__)

app.config.update(
    MONGODB_HOST='localhost',
    MONGODB_PORT=27017,
    MONGODB_DB='xetrapal-smritibase',
)

CORS(app)

if len(sys.argv) > 1:
    configfile = sys.argv[1]
else:
    configfile = "/opt/av-appdata/avxpal.json"
# me = MongoEngine(app)
api = Api(app)
parser = reqparse.RequestParser()
# apismriti = smriti.XetrapalSmriti.objects(naam="xpal-api")[0]
apismriti = karma.load_xpal_smriti(configfile)
# apismriti.observed_by = apismriti.name
# apismriti.save()
# apismriti.reload()
apixpal = Xetrapal(apismriti)
apixpal.dhaarana(wakarmas)
apixpal.dhaarana(fbkarmas)
apixpal.dhaarana(twkarmas)
apixpal.dhaarana(samvadkarmas)
app.logger = apixpal.logger


class ApiResource(Resource):
    def get(self, command=None):
        status = "success"
        try:
            if command is None:
                resp = ["Xetrapal is Running!"]
            elif command == "get_profile":
                resp = [json.loads(apixpal.smriti.to_json())]
            elif command == "get_config":
                resp = [karma.load_config_json(apixpal.configfile)]
            else:
                resp = "error: Unrecognized command"
                status = "error"
        except Exception as e:
            resp = "error: {} {}".format(type(e), str(e))
            status = "error"
        return jsonify({"resp": list(resp), "status": status, "urlbase": apismriti.urlbase, "datapath": apixpal.datapath})

    def post(self, command=None):
        status = "success"
        resp = []
        if command == "put_config":
            newconfig = request.get_json()
            apixpal.save_data_to_jsonfile(data=newconfig, filename=apixpal.configfile)
            resp = [karma.load_config_json(apixpal.configfile)]
        return jsonify({"resp": list(resp), "status": status, "urlbase": apismriti.urlbase, "datapath": apixpal.datapath})


api.add_resource(ApiResource, "/", endpoint="api-root")
api.add_resource(ApiResource, "/<string:command>", endpoint="api-command")


class XetrapalSessionResource(Resource):
    def get(self, session_id=None, session_name=None):
        status = "success"
        resp = []
        try:
            apixpal.session.save()
            apixpal.smriti.reload()
            apixpal.session.reload()
            apixpal.smriti.lastsession.reload()
            if session_id is not None:
                resp = [smriti.XetrapalSession.objects.with_id(session_id)]
            elif session_name is not None:
                resp = smriti.XetrapalSession.objects(
                    session_name=session_name)
            else:
                resp = smriti.XetrapalSession.objects()
        except Exception as e:
            resp = "error: {} {}".format(type(e), str(e))
            status = "error"
        return jsonify({"resp": resp, "status": status, "urlbase": apismriti.urlbase, "datapath": apixpal.datapath})


api.add_resource(XetrapalSessionResource, "/xetrapal_session",
                 endpoint="xetrapal_session")
api.add_resource(XetrapalSessionResource,
                 "/xetrapal_session/by_id/<string:session_id>", endpoint="xetrapal_session_id")
api.add_resource(XetrapalSessionResource,
                 "/xetrapal_session/by_name/<string:session_name>", endpoint="xetrapal_session_name")


class CommandResource(Resource):
    def post(self):
        resp = [request.get_json()]
        apixpal.logger.info("Received Command:\n{}".format(resp))
        op = apixpal.run_aadesh(copy.deepcopy(resp[0]))
        if type(op) != dict:
            op = str(op)
        resp.append(op)
        status = "success"
        return jsonify({"resp": resp, "status": status, "urlbase": apismriti.urlbase, "datapath": apixpal.datapath})


api.add_resource(CommandResource, "/command", endpoint="command")


if __name__ == '__main__':
    app.run(host="0.0.0.0")
