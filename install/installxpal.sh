echo "Installing Dependencies"
echo "Adding mongod community repo"
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt update && sudo apt dist-upgrade
sudo apt install python3-setuptools python3-pip
sudo apt-get install -y mongodb-org
sudo -H pip3 install ipython
echo "Cloning Xetrapal"
git clone https://gitlab.com/hackergram/xetrapal
cd xetrapal/
sudo -H python3 setup.py install
sudo chmod a+rwx -R /opt
