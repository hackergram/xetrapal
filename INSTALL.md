# How to install Xetrapal (dev-chitragupt)

## install python3
```
sudo apt install build-essential python3 python3-pip python3-dev
```

## Install MongoDB community edition
Install MongoDB Community edition - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

## Install geckodriver
Get a copy of the latest geckodriver binary for your system and put it in your system path - https://github.com/mozilla/geckodriver/releases

### Unzip the downloaded file and put it in the system path 
```
tar zxvf /path/to/file.tgz  
sudo cp /path/to/file /us/sbin 
```

## first clone the repository to your system :
```
git clone https://gitlab.com/hackergram/xetrapal
cd xetrapal
git checkout dev-chitragupt
```

## install the dependencies:
```
sudo -H pip3 install -r requirements.txt
sudo -H pip3 install ipython
```



## Now open a python window:
```
ipython3
```

## In ipython3
```
import xetrapal
a=xetrapal.karma.load_xpal_smriti("/opt/samvad-appdata/samvadxpal.conf") 
samvadxpal = xetrapal.Xetrapal(a) 
from xetrapal import wakarmas
samvadxpal.dhaarana(wakarmas) 
samvadxpal.add_astra("wabrowser",samvadxpal.get_browser()) 
samvadxpal.wa_login()                **(scan the qr code at this stage)** 
```

## You should now have a whatsapp logged in browser linked to xetrapal, now launch another ipython screen in another terminal
```
ipython3
```

## Once in python:
```
from xetrapal import api
api.app.run("0.0.0.0") 
```

## Now go to your browser
http://127.0.0.1/xetrapal



## Now you are having a graphical interface of the Xetrapal running in your browser console  


##  Now go back to the previous browser console and run these commands, to automate the whatsapp functions through Xetrapal
```
samvadxpal.wa_search_conversations("any group or conversation’s name") 
 ```
 
##  Now copy the output of the previous command we have launched and paste it in front of the q(or any variable) like
```
q = paste the output of the command here 
```

##  Now run these commands to add the conversations and messages of the contact/group to the xetrapal 
```
p=samvadxpal.wa_add_conversation_smriti(q) 
samvadxpal.wa_get_conv_messages(p) 
```

## Now reload your xetrapal window running in your browser console and check whether the messages/conversations are loaded
