# xetrapal (क्षेत्रपाल)

## What it is
हिन्दी एवं अन्य भारतीय भाशाओं में इंटरनेट से सामग्री को साझा करने एवं स्वतंत्र रूप से विश्लेषण करने के उपकरण

Orchestration and automation framework for web based task work, data analysis and mining, particularly in Indic languages

## Why it's needed 
With growing social media, everyone needs automation to keep up with the barrage of data coming at us. Xetrapal can help. 

## Who is maintaining
हैकरgram Residents in Naukuchiyatal, Uttarakhand, India (listener at hackergram dot org)


## How To install 
sh -c "$(curl -fsSL https://hackergram.org/xpal/installxpal.sh)"

## How to play

### Set up your config file

Configuration files for xetrapal are json files, with the following minimum configuration

```
{
      "Jeeva":{
        "name" : "Sabka Baap",
        "datapath" : "/opt/av-appdata/xetrapal-data",
        "sessionpathprefix" : "SabKaBaap-XpalSession",
        "urlbase" : "http://192.168.56.101/xetrapal"
      }
}
```

Make a file like so somewhere on your filesystem
Then to run the api

```
cd xetrapal
python3 bin/xpalapi.py <pathtoconfig>
```

Or if you want to go interactive

```
# In ipython3

from xetrapal import api

api.app.run("0.0.0.0","5001") #Runs the API on port 5001 of all interfaces

api.apixpal.run_aadesh(messagejson) #Processes in the same format as received by the /command interface
```

Message JSON format

```
{ 
	"msg":"get",
	"func":"fb_post",
	"args":[],
	"kwargs": { 
		"post": {
			"text":"Aaya aaya Xetrapal aaya!"
			}
		}
}
```
